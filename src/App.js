import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  constructor(props){
    super(props);
  
    this.state = {
      todos: todosList,
      input: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleNewItem = this.handleNewItem.bind(this);
    this.clearCompleted = this.clearCompleted.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleComplete = this.handleComplete.bind(this);
    this.textInput = React.createRef();
    
  }
  handleInputChange(event){
    this.setState({
      input: event.target.value
    })
  }
  handleNewItem(event){
    event.preventDefault();
    
    const newItem = this.state.todos.concat({"userId": 1, "id": this.state.todos.length + 1, "title": this.state.input, "completed": false});
    this.setState({
      todos: newItem
    });
    this.textInput.current.value = "";
    
  }
  handleDelete = (key) => (event) => {
    event.preventDefault();
    let newArray = [];
    this.state.todos.forEach(todo => {
      if(todo.id !== key){
        newArray.push(todo);
      }
    })
    return this.setState({
      todos: newArray
    })
  }
  handleComplete = (key) => (event) => {
    
    const newArray = [];
    this.state.todos.forEach(todo => {
      if(todo.id === key){
        todo.completed = !todo.completed
    }
    newArray.push(todo);
  })
    return this.setState({
      todos: newArray
    })
  }
  clearCompleted = (event) => {
    event.preventDefault();
    const newArray = [];
    this.state.todos.forEach(todo => {
      if(todo.completed === false){
        newArray.push(todo);
      }
      
    })
    return this.setState({
      todos: newArray
    })
  }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handleNewItem}>
            
            <input ref={this.textInput} className="new-todo" placeholder="What needs to be done?" autoFocus onChange={this.handleInputChange}/>
          </form>
        </header>
        <TodoList todos={this.state.todos} deleteFunc={this.handleDelete} completedFunc={this.handleComplete}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button onClick={this.clearCompleted} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  
  
 

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" onChange={this.props.completedFunc} defaultChecked={this.props.completed}/>
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.deleteFunc}/>
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          
          {this.props.todos.map(todo => (
            <TodoItem completedFunc={this.props.completedFunc(todo.id)} deleteFunc={this.props.deleteFunc(todo.id)} key={todo.id} title={todo.title} completed={todo.completed} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
